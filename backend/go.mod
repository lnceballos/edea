module gitlab.com/edea-dev/edea/backend

go 1.16

require (
	github.com/Microsoft/go-winio v0.4.16 // indirect
	github.com/alecthomas/chroma v0.8.2
	github.com/alecthomas/colour v0.1.0 // indirect
	github.com/alecthomas/repr v0.0.0-20201120212035-bb82daffcca2 // indirect
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/coreos/go-oidc v2.2.1+incompatible // indirect
	github.com/coreos/go-oidc/v3 v3.0.0
	github.com/denisenkom/go-mssqldb v0.9.0 // indirect
	github.com/dlclark/regexp2 v1.4.0 // indirect
	github.com/gitsight/go-vcsurl v1.0.0
	github.com/gliderlabs/ssh v0.3.1 // indirect
	github.com/go-git/go-git/v5 v5.2.0
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/google/uuid v1.2.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/jackc/pgmock v0.0.0-20201204152224-4fe30f7445fd // indirect
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/kevinburke/ssh_config v0.0.0-20201106050909-4977a11b4351 // indirect
	github.com/lib/pq v1.9.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	github.com/pquerna/cachecontrol v0.0.0-20201205024021-ac21108117ac // indirect
	github.com/rs/zerolog v1.20.0
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/square/go-jose v2.5.1+incompatible // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/xanzy/ssh-agent v0.3.0 // indirect
	github.com/yuin/goldmark v1.3.1
	github.com/yuin/goldmark-highlighting v0.0.0-20200307114337-60d527fdb691
	github.com/yuin/goldmark-meta v1.0.0
	golang.org/dl v0.0.0-20210204224843-1557c60ec592 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	golang.org/x/oauth2 v0.0.0-20210201163806-010130855d6c
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/square/go-jose.v2 v2.5.1
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	gorm.io/datatypes v1.0.0
	gorm.io/driver/mysql v1.0.4 // indirect
	gorm.io/driver/postgres v1.0.8
	gorm.io/driver/sqlite v1.1.4 // indirect
	gorm.io/driver/sqlserver v1.0.6 // indirect
	gorm.io/gorm v1.20.12
)
