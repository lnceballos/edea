// SPDX-FileCopyrightText:  2020  Elen Eisendle
// SPDX-License-Identifier: EUPL-1.2

package repo

// https://try.gitea.io/wumi/vue-element-admin/raw/branch/master/README.md
// https://try.gitea.io/api/v1/repos/wumi/vue-element-admin -> key "default_branch"
