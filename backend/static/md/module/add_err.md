---
Title: Could not add module to bench
Template: index.tmpl
---
# Oopsie

{{if .Error}}
<p>Error: {{.Error}}</p>
{{end}}
