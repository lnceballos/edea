// SPDX-FileCopyrightText:  2020  Elen Eisendle
// SPDX-License-Identifier: EUPL-1.2

package util

import "github.com/gorilla/schema"

var (
	// FormDecoder cache for all views
	FormDecoder = schema.NewDecoder()
)
