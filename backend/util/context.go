// SPDX-FileCopyrightText:  2020  Elen Eisendle
// SPDX-License-Identifier: EUPL-1.2

package util

var (
	// ContextKey for user information stored for logged in users
	UserContextKey = struct{}{}
)
