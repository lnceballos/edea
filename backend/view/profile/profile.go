// SPDX-FileCopyrightText:  2020  Elen Eisendle
// SPDX-License-Identifier: EUPL-1.2

package profile

import "net/http"

func User(w http.ResponseWriter, r *http.Request) {
	// view a profile
}

func Me(w http.ResponseWriter, r *http.Request) {
	// view my own profle
}

func Change(w http.ResponseWriter, r *http.Request) {
	// change own profile
}
