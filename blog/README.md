# Blog

This contains the blog posts which have been published, or also those which are still work-in-progress.
See `content/` for the posts and `templates/` for the custom template.

The static site generator used is called [zola](https://github.com/getzola/zola).
