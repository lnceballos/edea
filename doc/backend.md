# Backend Architecture Overview

```mermaid
graph TD
	A[Client] --> |Authorized request| B(IAP)
    B <--> |No valid token?| X(Identity Provider)
	B --> |Authorized or public request| C{EDeA}
    A --> |Basic request| B 
	C -->|Repositories| D[(Filesystem)]
	C -->|Metadata| E[(PostgreSQL)]
	C -->|Rendered Readmes| F[(Cache)]
```

## Storage

User data is stored in the database. Provide an easy way for admins to export the data for backup purposes. The goal here is not to supplement or replace database backups, but to give a dump which can be compressed and diffed for small incremental backups. Those might be more difficult to restore (provide a tool for that!) and especially more time-intensive. But it would be a more space-saving approach, when a days downtime is okay to have (also as a second line of defense against data loss).

In any case, a caching system will be implemented, which can then be gradually used for parts of EDeA where it fits the use-case. Also caching in this case does not mean caching queries and requests, but pre-rendering data which will be served to users like readmes. A filesystem is good enough for now in handling the load that is expected.

### Modules

Caching and storage will be handled by the filesystem for now. Depending on where we want to store additional metadata and/or how much is accumulated within projects it would be good to store them outside the database. There needs to be benchmarking done when the time comes as to how well Postgres performs with JSON(B) data storage with our use-case. As soon as we have some data with the first beta tests, we can then assess which actions need to be taken (if any) to increase the performance.

### Database

The only database supported is PostgreSQL. It provides the best SQL standard compliance, no complicated licensing situation (e.g. like MySQL) and also runs basically anywhere. We don't expect enough load on self-hosted instances that much administration of the database would be necessary, which means apart from setting it up it won't be much more work than e.g. SQLite.

## External Repositories

Currently they are stored in the filesystem, other solutions might be viable in the future, but this scales reasonably well for now. Caching of files to be displayed in the interface when browsing will also be implemented to provide a speed-up.

## Authentication

Done with JWT and OAuth. The goal is to support one self-hosted solution (Ory Kratos/Hydra) and one SaaS Provider with a reasonable free tier (e.g. 0auth) for people who only quickly want to set something up without hosting all the infrastructure themselves.
EDeA will only the basic and necessary features, as there's more than enough open source solutions to handle more complex aspects of the topic. Like Ory Oathkeeper, there's also other Identity Proxy solutions that can be easily integrated. Less re-inventing the wheel is always good and gives people choice to fit self-hosted variants into their own ecosystem. Also it means a lot less maintenance load for us.

## Authorization

Same as above, basic features and endpoints will be implemented, including examples for open source solutions. This can again be done by a proxy or we might also directly implement support for a solution like Open Policy Agent.

## API

Standard REST APIs will be provided and also used by the JavaScript utilizing web-interface. There will also be the fallback routes, but they should not be used in any API manner, as they render the portal server-side.
Documentation and example tools will also be provided when the first web-prototype is done, as everything is in flux until then.

## Translation

Translation strings will be stored in configuration files and loaded at startup (hot loading may be implemented later). This makes it easy for contributers to add translation strings. More details TBD after we had the call with Translate House. For now especially user-facing errors must be defined and not just strings so that we can handle this part easily later.
