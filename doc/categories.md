# Categories

Class hierachies of categories. All attributes of a parent are inherited. Child-attributes are hidden while browsing a parent category.

Need a web-interface to define new categories, should define standard categories and maintain them in the repository.

It will be important to not define too many fields but also find out which are the most important ones. In addition we need the possibility to add custom fields which should at least be full-text searchable.

## Example

```mermaid
classDiagram
    class SubCircuit {
        +int TrackWidth
        +int BoardClass
        +int Height
        +int Width
        +int CopperLayers
        +int CopperThickness
        +int ComponentLayers
        +int ComponentCount
        +int UniqueComponentCount
        +bool LabTested
    }

    class PowerSupply {
        +bool Isolated
        +int VinMax
        +int VinMin
        +int VoutMin
        +int VoutMax
        +int Iout
    }

    class Microcontroller {
        +string MCUName
        +string Architecture
        +int FlashSize
        +int RAMSize
        +int Frequency
    }

    class Switching {
        +int Frequency
    }

    class Linear {
        +int MinimumDropout
    }

    class Undefined {
        +int ShortDescription
    }

    SubCircuit <|-- PowerSupply
    SubCircuit <|-- Microcontroller
    SubCircuit  <|-- Undefined
    PowerSupply <|-- Switching
    PowerSupply <|-- Linear
    PowerSupply <|-- SpecialPurpose
```
