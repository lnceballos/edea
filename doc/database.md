# Tables

Please mirror any changes in the code in this file at the time of commit.

## Overview of the tables

```mermaid
classDiagram
    User "1" --> "1" Profile
    Bench "1" --> "1..*" BenchModule
    Module "1" --> "1" Repository
    Module "1" --> "1" Category
    Category "1" --> "1" Category
    User "1" --> "1..*" Module
    User "1" --> "1..*" Bench

    class User {
        +uuid id [pk]
        +uuid auth_uuid
        +string handle
        +timestamp created
        +bool is_admin
        +uuid profile_id [fk]
    }

    class BenchModule {
        +uuid id [pk]
        +string name
        +string description
        +json configuration
        +uuid project_id [fk]
        +uuid bench_id [fk]
    }

    class Repository {
        +uuid id [pk]
        +string url
        +string type
        +string location
        +time updated
        +time created
    }

    class Profile {
        +uuid id [pk]
        +string display_name
        +string location
        +string biography
        +string avatar
    }

    class Module {
        +uuid id [pk]
        +uuid user_id [fk]
        +string repo_url
        +string name
        +string description
        +uuid category_id [fk]
        +map category_fields
    }

    class Bench {
        +uuid id [pk]
        +uuid user_id [fk]
        +bool active
        +bool public
        +time created
        +string name
        +string description
    }

    class Category {
        +uuid id [pk]
        +uuid parent_id [fk]
        +string name
        +string description
        +string[] mandatory_fields
        +string[] optional_fields
    }
```

## Constraints

### Table reads

Return everything that's public or belongs to the current user.

### Row Update

Allow update if the current user is an (Organization, Module, Bench) admin or owns the row.

- Allow making a Bench public only if all referenced Modules are public


### Row Insert

Allow on Bench, Organization, Module by default.

Constraints on: BenchMember, OrganizationMember, ModuleMember
Special cases: Category, User, Profile
