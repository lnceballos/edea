#!/usr/bin/env bash
# SPDX-FileCopyrightText:  2020  Elen Eisendle
# SPDX-License-Identifier: EUPL-1.2
set -v
# This file is for internal use only

tar cfav snapshot.tar.zst public_html
scp snapshot.tar.zst automated.ee:
ssh automated.ee tar xvf snapshot.tar.zst
# https://fully.automated.ee:8081/
