#!/usr/bin/env bash
# SPDX-FileCopyrightText:  2020  Elen Eisendle
# SPDX-License-Identifier: EUPL-1.2

# Turn on shell verbose mode
set -v

npm version && npm install && npm audit fix

