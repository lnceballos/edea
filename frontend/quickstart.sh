#!/usr/bin/env bash
# SPDX-FileCopyrightText:  2020  Elen Eisendle
# SPDX-License-Identifier: EUPL-1.2

source options.txt
set -e
set -v

./install-dependencies.sh 
./build-fe.sh 
./serve.sh & 
SRV_PID=$!
sleep 3s
xdg-open "$URL"

echo "Web server is running under PID $SRV_PID"
fg 
